/*
 ============================================================================
 Name        : Monte-Carlo.c
 Author      : Demid
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include<math.h>

//������ ���������� ��������� �� �����
void print_coord_vert(const double [][2], const int);
//�������������� ������� �� ������ �����-�����.
void MK_method(double coord[][2], const int num_par, const int num_it,
				const double alpha, const double beta, const double T,
				const double step, const int tau);
//����� ��������� �� �����
void print_coord_vert(const double [][2], const int);

const int num_par_1 = 24;
const double alpha = 1, beta = 1;
const double init_eps = 1;

const double h = 0.01, epselon = 1E-12;
const int lim_it_1 = 10000, lim_it_2 = 100;

int main() {

	double coordinats[num_par_1][2], energy = 0;

	print_coord_vert(coordinats, num_par_1);

	return 0;
}


void print_coord_vert(const double coord[][2], const int num){
	int i = 0;
	for(i = 0; i < num; i++){
			printf("%-12f%-12f\n", coord[i][0], coord[i][1]);
	}
	printf("\n");
}

void reading_file(double coord[][2], const int num_par, double *energy, const char *name ){
	float h_val = 0;
	int i = 0, j = 0;
	FILE *sfPtr;

	if( (sfPtr = fopen(name,"r")) == NULL ){
			printf("File coudn't be opened.\n");
		} else {
			for(i = 0; i < num_par; i++){
				for(j = 0; j < 2; j++){
					fscanf(sfPtr,"%f",&h_val);
					printf("%f\n",h_val);
					coord[i][j] = h_val;
				}
			}
			fscanf(sfPtr,"%f",&h_val);
			*energy = h_val;
		fclose(sfPtr);
	}
}


void MK_method(double coord[][2], const int num_par, const int num_it,
				const double alpha, const double beta, const double T,
				const double step, const int tau, int *En, char *name, const int per){
	int i = 0, j = 0, k = 0, a = 0;
	double  delta_En = 0, delta_en_n = 0;
	double delta_x = 0, delta_y = 0, h_val = 0, h_val_1 = 0;
	char st[20], st1[20], st2[20];
	char *st3;
	sprintf(st,".txt");
	st3 = st2;
	FILE *sfPtr;

	for(i = 0; i < num_it; i++){
		a = i%per;
		if( a == 0 ){
			sprintf(st1,"_%d",i);
			st3 = strcpy(st3,name);
			st3 = strcat(st3,st1);
			st3 = strcat(st3,st);
			sfPtr = fopen(st3,"w");
		}
		for(j = 0; j < num_par; j++){

			//��������� ���������� ������
			h_val = rand();
			delta_x = step*((h_val/RAND_MAX) - 0.5);
			h_val = rand();
			delta_y = step*((h_val/RAND_MAX) - 0.5);

			//���������� �������� �������
			delta_En = 0;
			for(k = 0; k < num_par; k++){
				if( j != k){
					delta_En -= 1/sqrt(pow((coord[j][0] - coord[k][0]),2) +  pow((coord[j][1] - coord[k][1]),2));
					delta_En += 1/sqrt(pow((coord[j][0] - coord[k][0] + delta_x),2) +  pow((coord[j][1] - coord[k][1] + delta_y),2));
				}
			}
			delta_En *= alpha;
			delta_En -= beta*(pow(coord[j][0],2) + pow(coord[j][1],2) -  pow((coord[j][0] + delta_x),2) - pow((coord[j][1] + delta_y),2));

			//�������� �������
			h_val = rand();
			h_val /= RAND_MAX;
			h_val_1 = fmin(1, exp(-(delta_En/T)))/tau;
			if( (h_val < h_val_1) || (h_val_1 == 1) ){
				coord[j][0] += delta_x;
				coord[j][1] += delta_y;
				delta_en_n += delta_En;
			}
			if( a == 0 ){
				fprintf(sfPtr,"%-15f%-15f\n",coord[j][0],coord[j][1]);
			}
		}
		delta_en_n /= num_par;
		*En += delta_en_n;
		if( a == 0 ){
			printf("iter = %-6dpot_en = %-10f",i,*En);
			fclose(sfPtr);
		}
	}
}
